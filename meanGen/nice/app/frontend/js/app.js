'use strict';

var app = angular.module('nice', [
  'ngResource',
  'ngCookies',
  'ngTouch',
  'ngSanitize',
  'ui.bootstrap'
]);


app.config(['$locationProvider', '$httpProvider',
  
  function ($locationProvider, $httpProvider) {


    var isLoggedIn = function ($q, $timeout, $http, $rootScope, $location) {
        var deferred = $q.defer();
        $http.get('/signedin').success(function (user) {
            if (user !== '0') {
                $rootScope.isSignedIn = true;
                $rootScope.currentUser = user;
                $timeout(deferred.resolve, 0);
            } else {
                $rootScope.isSignedIn = false;
                $rootScope.currentUser = {};
                $timeout(function() { deferred.reject();}, 0);
                $location.url('/');
            }
        });
        return deferred.promise;
    };


  $httpProvider.interceptors.push('InterceptorService');

  

  $locationProvider.html5Mode(true);

}]);