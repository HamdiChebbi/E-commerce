var models= require('../models');
var express = require('express');
var router = express.Router();
var mongoose= require('mongoose') ; 

/* GET users listing. */
router.get('/', function(req, res, next) {
    res.render('main/home') ; 
});

/* GET signup page. */
router.get('/signup', function(req, res, next) {
    res.render('accounts/signup') ; 
});

router.get('/login', function(req, res, next) {
    res.render('accounts/login') ; 
});

// Ajouter un nouveau utilisateur 
router.post('/signup',function(req,res,next){
    var user = new models.user(); 
    user.profile.name=req.body.name; 
    user.password=req.body.password ; 
    user.email=req.body.email; 
    
    // vérification de l'existance !! 
    models.user.findOne({email:req.body.email}, function(err,exixtingUser){
        if(exixtingUser){
            console.log(req.body.email+ " existe déja !!") ; 
            return res.redirect('/signup') ; 
        } else { // sinon sauvegarder l'user
            user.save(function(err){
        if(err) return next(err); // vérification de la dupplication 
        res.json('Nice User added !!') ; 
    });
            
        }
    });    
});
module.exports = router;
